package com.assignment1;

import edu.unh.cs.treccar_v2.Data;
import edu.unh.cs.treccar_v2.read_data.DeserializeData;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.FSDirectory;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Iterator;

/**
 * An indexer for paragraphs.
 *
 * @author Samantha Piatt
 * @since Sept 04, 2018
 */
class IndexParagraphs {
    private IndexWriter writer = null;
    private Analyzer analyzer;

    /**
     * Construct and indexer for paragraphs by path, using an Analyzer.
     *
     * @param directory The directory to save the indexed files to.
     * @param analyzer The analyzer to write with.
     */
    IndexParagraphs(String directory, Analyzer analyzer){
        this.analyzer = analyzer;
        open(directory);
    }

    /**
     * Open the index writer to a specific index path.
     *
     * @param directory The path to save the index files in.
     */
    private void open(String directory) {
        try{
            if (writer != null) close();
            Path dir = FileSystems.getDefault().getPath(directory);
            IndexWriterConfig config = new IndexWriterConfig(analyzer);
            writer = new IndexWriter(FSDirectory.open(dir), config);
        } catch (Exception e){
            System.err.println("Error: " + e.toString());
            writer = null;
        }
    }

    /**
     * Close the index writer.
     */
    void close(){
        if(writer != null){
            try{
                writer.close();
                writer = null;
            } catch (Exception e){
                System.err.println("Error: " + e.toString());
                writer = null;
            }
        }
    }

    /**
     * Add paragraphs from document by path.
     *
     * @param path The path file path containing paragraphs to add to the index.
     */
    void addDoc(String path){
        try {
            FileInputStream fileInputStream2 = new FileInputStream(new File(path));
            Iterator<Data.Paragraph> paragraphIterator = DeserializeData.iterParagraphs(fileInputStream2);
            while (paragraphIterator.hasNext()) {
                Document doc = new Document();
                Data.Paragraph p = paragraphIterator.next();
                doc.add(new StringField("id", p.getParaId(), Field.Store.YES));
                doc.add(new TextField("paragraph", p.getTextOnly(), Field.Store.YES));
                writer.addDocument(doc);
            }
        } catch (Exception e){
            System.err.println("Error: " + e.toString());
        }
    }
}
