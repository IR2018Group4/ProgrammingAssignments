package com.assignment1;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;

import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.Arrays;

/**
 * Search application to handle cli input queries and output search results.
 *
 * @author Samantha Piatt
 * @since Sept 04, 2018
 */
public class Search {
    private static final StandardAnalyzer ANALYZER = new StandardAnalyzer();
    private static final String PARAGRAPHS = "data/test200/test200-train/train.pages.cbor-paragraphs.cbor";
    private static final String INDEXPATH = "data/index-directory";

    /**
     * Search Application.
     *
     * @param args Command line arguments, in the form [query] [count].
     */
    public static void main(String[] args ){
        // Check command line arguments
        if (args.length < 2) {
            System.out.println("Invalid number of parameters. Use format: <query> <count>");
            System.exit(-1);
        }
        String query = args[0];
        int count = Integer.parseInt(args[1]);

        // Build an index, if it isn't there already
        if (!Files.exists(FileSystems.getDefault().getPath(INDEXPATH))){
            System.out.println("--Building Index--");
            IndexParagraphs luceneIndex = new IndexParagraphs(INDEXPATH, ANALYZER);
            luceneIndex.addDoc(PARAGRAPHS);
            luceneIndex.close();
        }

        // Perform search
        System.out.println("\n--Querying Index for '" + query + "', top " + count + "--");
        SearchParagraphs searchEngine = new SearchParagraphs(INDEXPATH, ANALYZER);
        TopDocs result = searchEngine.performSearch(query, count);
        if(result == null){
            System.out.println("No results found.");
            System.exit(-1);
        }

        // Print results
        ScoreDoc[] hits = result.scoreDocs;
        for(int i = 0; i < hits.length; i++){
            Document d = searchEngine.getDocument(hits[i].doc);
            System.out.println("Result #" + (i + 1) + ": Document id " + hits[i].doc + ", Score " + hits[i].score);
            System.out.println(Arrays.toString(d.getValues("paragraph")) + "\n");
        }
    }
}
