package com.assignment1;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;

import java.nio.file.FileSystems;
import java.nio.file.Path;

 /**
 * A search engine for returning paragraphs results.
  *
  * @author Samantha Piatt
  * @since Sept 04, 2018
 */
class SearchParagraphs {
    private IndexSearcher searcher = null;
    private QueryParser parser = null;
    private final String inputPath;
    private final Analyzer analyzer;

     /**
      * Construct a SearchParagraph object that searches a specific index directory using an analyzer.
      *
      * @param directory The index directory to search in.
      * @param analyzer The analyzer object to use for searching.
      */
    SearchParagraphs(String directory, Analyzer analyzer){
         this.inputPath = directory;
         this.analyzer = analyzer;
         openEngine();
     }

     /**
      * Open the engine for searching.
      */
    private void openEngine(){
        try{
            Path dir = FileSystems.getDefault().getPath(inputPath);
            searcher = new IndexSearcher(DirectoryReader.open(FSDirectory.open(dir)));
            parser = new QueryParser("paragraph", analyzer);
        } catch (Exception e){
            System.err.println("Error: " + e.toString());
        }
    }

     /**
      * Perform a search given a query string and return the top n results.
      *
      * @param queryString The query terms to search for.
      * @param n The number of results to return.
      * @return A TopDocs object containing search results.
      */
    TopDocs performSearch(String queryString, int n){
        TopDocs result;
        try{
            result = searcher.search(parser.parse(queryString), n);
        } catch (Exception e){
            System.err.println("Error: " + e.toString());
            return null;
        }
        return result;
    }

     /**
      * Return a document object from a search given the document id
      *
      * @param docId The document id number to return.
      * @return A Document object.
      */
    Document getDocument(int docId){
        Document doc;
        try{
            doc = searcher.doc(docId);
        } catch (Exception e){
            System.err.println("Error: " + e.toString());
            return null;
        }
        return doc;
    }
}
