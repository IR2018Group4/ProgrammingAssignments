# Assignment 1
* Install and Run (Instructions are the same as below.): [install.pdf](documentation/install.pdf)
* Results and Analysis: [results.pdf](documentation/results.pdf)

## Program Specifications
This program is built with Maven and should be able to automatically generate itself. I have included a shell script (`run.sh`) that can compile, build an index, and then run each of the three searches required for programming assignment 1.

### Requirements
This program was created for and tested with:
- Java 1.8.0
- Maven 1.7

## Compile and Search
**Run all commands from the Assignment01 directory.**

### Automatic Compile and Search
There is an included `run.sh` shell script to automatically compile the `LuceneSearch.jar` file and run searches against it, outputting to System.out.

### Manual CLI Searching
Searches can be done manually using the cli command:

```bash
    java -jar LuceneSearch.jar "power nap benefits" 10
```

### Manual JAR Compile
The compiled `LuceneSearch.jar` file is already included in the repo. However, it can be regenerated in one of the following ways:
* Delete `LuceneSearch.jar` and run the `run.sh` script, which will generate it again and then perform a search.
* Use the CLI commands below to generate only the jar files, without executing the search:

```bash
    mvn clean compile assembly:single
    mv target/LuceneSearch-1.0-jar-with-dependencies.jar LuceneSearch.jar
    mvn clean
```