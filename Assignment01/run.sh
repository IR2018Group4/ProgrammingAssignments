#!/usr/bin/env bash
## Compile, index, and run sample searches.
## Author: Samantha Piatt, Group 4
## Date: Sept 04, 2018

if [ ! -f LuceneSearch.jar ]
then
    # Create jar file if it didn't exist and name it LuceneSearch.jar
    mvn clean compile assembly:single
    mv target/LuceneSearch-1.0-jar-with-dependencies.jar LuceneSearch.jar
    mvn clean
fi

# Execute searches
java -jar LuceneSearch.jar "power nap benefits" 10
java -jar LuceneSearch.jar "whale vocalization production of sound" 10
java -jar LuceneSearch.jar "pokemon puzzle league" 10

# Clean out index files
rm -rf data/index-directory/